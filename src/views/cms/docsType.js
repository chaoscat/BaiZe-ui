export const DOCS_TYPE = [{
  value: 'txt',
  label: '文本'
}, {
  value: 'markdown',
  label: 'markdown'
}, {
  value: 'world',
  label: 'world'
}, {
  value: 'excel',
  label: 'excel'
}, {
  value: 'ppt',
  label: '幻灯片'
}, {
  value: 'img',
  label: '画图'
}, {
  value: 'mindMapping',
  label: '思维导图'
}, {
  value: 'bpmn',
  label: '流程图'
}, {
  value: 'code',
  label: '代码'
}]